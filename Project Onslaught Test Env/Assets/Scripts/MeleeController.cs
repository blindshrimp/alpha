﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeController : MonoBehaviour
{
    public float attackRate;
    public int damage;
    public float hitSpeed;
    public float waitTime;

    private float nextHit;
    private Quaternion startRot;
    private Quaternion endRot;

    private MeleeDamageController meleeDamageController;

    private void Start()
    {
        nextHit = 0;
        startRot = transform.rotation;
        endRot = Quaternion.Euler(0, 0, -45);

        meleeDamageController = gameObject.GetComponentInChildren<MeleeDamageController>();
        meleeDamageController.damage = damage;
    }

    // Update is called once per frame
    void Update ()
    {           
        if (Input.GetMouseButtonDown(0) && Time.time > nextHit)
        {
            meleeDamageController.isHitting = true;
            StartCoroutine("Hit");
        }

        //Change to face  mouse
        if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x <  transform.parent.position.x)
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 180, transform.rotation.eulerAngles.z);
        else transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0, transform.rotation.eulerAngles.z);
    }

    IEnumerator Hit()
    {
        transform.rotation = Quaternion.Lerp(startRot, endRot, hitSpeed * Time.deltaTime);
        yield return new WaitForSeconds(waitTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, startRot, hitSpeed * Time.deltaTime);
        nextHit = Time.time + attackRate;
        meleeDamageController.isHitting = false;
    }
}
