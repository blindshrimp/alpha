﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    [Header("Gun Stats")]
    public int maxAmmo;
    public int currentAmmo;
    public float fireRate;
    public int damage;
    public bool isAutomaticWeapon;
    public bool isEquipted;
    public Sprite playerSprite;
    public Vector3 firePointLocation;
    public Vector2 boxColliderSize;
    public float nextFire;
    public bool isShotgun;
    public string objectPoolerName;
    public float bulletSpeed;

    private ObjectPooler objPooler;

    private void Start()
    {
        objPooler = GameObject.Find(objectPoolerName).GetComponent<ObjectPooler>();
        currentAmmo = maxAmmo;
        nextFire = 0;
    }

    public void Shoot(Vector3 firePos)
    {
        if (isEquipted)
        {
            if (!isShotgun)
                ShootSingle(firePos);
            else
                ShootShotgun(firePos);
        }        
    }

    private void ShootSingle(Vector3 firePos)
    {
        //Instantiate shot
        GameObject bullet = objPooler.GetPooledObject();

        if (bullet == null)
            return;

        bullet.transform.position = firePos;
        bullet.transform.rotation = Quaternion.identity;        
        bullet.SetActive(true);

        BulletMover bulletMover = bullet.GetComponent<BulletMover>();
        bulletMover.damage = damage;
        bulletMover.speed = bulletSpeed;
        bulletMover.SetMoveDirection();

        //Lower ammo
        currentAmmo--;

        //Increase nextFire
        nextFire = Time.time + fireRate;
    }

    private void ShootShotgun(Vector2 firePos)
    {
        //Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        ////Get shot direction & normalize
        //Vector2 shotDirection = mousePos - firePos;
        //shotDirection.Normalize();

        ////Instantiate shot
        //GameObject bullet = Instantiate(objBullet, firePos, Quaternion.identity);
        //BulletMover bulletMover = bullet.GetComponent<BulletMover>();
        //bulletMover.damage = damage;

        ////Lower ammo
        //currentAmmo--;

        ////Increase nextFire
        //nextFire = Time.time + fireRate;
    }
}
