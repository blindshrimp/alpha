﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{

    public int startingWave;
    public int currentWave;
    public float waveTime;
    public bool waveStarted;
    public GameObject startWaveText;
    public GameObject obj_waveText;

    private ScoreManager scoreManager;
    private EnemySpawnController spawnController;
    private GameObject[] activeEnemies;
    private GameObject player;
    private Text waveText;

    private void Start()
    {
        waveTime = 0;
        scoreManager = GetComponent<ScoreManager>();
        spawnController = GetComponent<EnemySpawnController>();
        waveText = obj_waveText.GetComponent<Text>();
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (waveStarted && startWaveText.activeInHierarchy)
        {
            startWaveText.SetActive(false);
            waveText.text = "Wave: " + currentWave;
            obj_waveText.SetActive(true);
        }

        if (!waveStarted && !startWaveText.activeInHierarchy)
        {
            startWaveText.SetActive(true);
            obj_waveText.SetActive(false);
        }


        if (Input.GetKeyDown(KeyCode.Space) && !waveStarted)
            waveStarted = true;


        if (waveStarted)
        {
            waveTime += Time.deltaTime;

            activeEnemies = GameObject.FindGameObjectsWithTag("Enemy");

            if (spawnController.totalEnemiesToSpawn == spawnController.currentEnemiesSpawned && activeEnemies.Length <= 0)
                EndWave();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    private void EndWave()
    {
        waveStarted = false;
        currentWave++;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
