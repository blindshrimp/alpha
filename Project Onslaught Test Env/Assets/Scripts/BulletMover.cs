﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMover : MonoBehaviour {

    Vector3 moveDirection;
    public float speed;
    public int damage;

    private void Start()
    {
        moveDirection = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position);
        moveDirection.z = 0;
        moveDirection.Normalize();
    }

    public void SetMoveDirection()
    {
        moveDirection = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position);
        moveDirection.z = 0;
        moveDirection.Normalize();
    }

    private void Update()
    {
        transform.position = transform.position + moveDirection * speed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Border")
            gameObject.SetActive(false);

        if (collision.gameObject.tag == "House")
            gameObject.SetActive(false);

        if (collision.gameObject.tag == "Car")
            gameObject.SetActive(false);

        if (collision.gameObject.tag == "Workbench")
            gameObject.SetActive(false);

        if (collision.gameObject.tag == "Radio")
            gameObject.SetActive(false);

        if (collision.gameObject.tag == "Gun Chest")
            gameObject.SetActive(false);

        if (collision.gameObject.tag == "Treeline")
            gameObject.SetActive(false);
    }
}
