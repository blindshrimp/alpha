﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed;
    public int health;
    public float jump;
    public Text healthText;

    private Rigidbody2D rb2D;
    private GameObject sceneManager;
    private SceneController sceneController;

    private Vector3 startPosition;
    private Quaternion startRotation;
    private int baseHealth;


    private void Start()
    {
        sceneManager = GameObject.Find("Scene Manager");
        sceneController = sceneManager.GetComponent<SceneController>();
        rb2D = GetComponent<Rigidbody2D>();
        startPosition = transform.position;
        startRotation = transform.rotation;
        baseHealth = health;
    }

    private void Update()
    {
        healthText.text = "Health: " + health;

        //Change to face  mouse
        if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x < transform.position.x)
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 180, transform.rotation.eulerAngles.z);
        else transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0, transform.rotation.eulerAngles.z);

        if (health <= 0)
            DeathSequence();
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        rb2D.AddForce(movement * speed * Time.fixedDeltaTime);         
    }

    #region OnCollision

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if (collision.gameObject.transform.position.x > transform.position.x)
                JumpBack();
            else
                JumpForward();
        }
    }


    #endregion


    private void JumpBack()
    {
        rb2D.AddForce(Vector2.left * jump * speed * Time.deltaTime);
    }

    private void JumpForward()
    {
        rb2D.AddForce(Vector2.right * jump * speed * Time.deltaTime);
    }

    private  void DeathSequence()
    {
        //reset player position
        transform.position = startPosition;
        transform.rotation = startRotation;

        health = baseHealth;
    }
}
