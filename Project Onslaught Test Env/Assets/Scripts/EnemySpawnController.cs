﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController : MonoBehaviour
{
    [Header("Spawns")]
    public int totalEnemiesToSpawn;
    public GameObject[] spawnPoints;

    [Header("Enemies")]
    public string basicZombiePoolName;
    public GameObject[] basicZombies;
    public string fastZombiePoolName;
    public GameObject[] fastZombies;
    public string fatZombiePoolName;
    public GameObject[] fatZombies;

    public int currentEnemiesSpawned { get; private set; }

    private ObjectPooler op_BasicZombies;
    private bool firstSpawn_BasicZombies;

    private ObjectPooler op_FastZombies;
    private bool firstSpawn_FastZombies;

    private ObjectPooler op_FatZombies;
    private bool firstSpawn_FatZombies;

    private SceneController sceneController;
    private bool spawning;

    // Use this for initialization
    void Start()
    {
        sceneController = GameObject.Find("Scene Manager").GetComponent<SceneController>();

        op_BasicZombies = GameObject.Find(basicZombiePoolName).GetComponent<ObjectPooler>();
        op_FastZombies = GameObject.Find(fastZombiePoolName).GetComponent<ObjectPooler>();
        op_FatZombies = GameObject.Find(fatZombiePoolName).GetComponent<ObjectPooler>();

        firstSpawn_BasicZombies = true;
        firstSpawn_FastZombies = true;
        firstSpawn_FatZombies = true;

        //StartCoroutine("SpawnBasicZombies");
        //StartCoroutine("SpawnFastZombies");
        //StartCoroutine("SpawnFatZombies");       
    }

    private void Update()
    {
        if(sceneController.waveStarted && !spawning)
        {
            StartCoroutine("SpawnBasicZombies");
            StartCoroutine("SpawnFastZombies");
            StartCoroutine("SpawnFatZombies");
            spawning = true;
        }

        if(!sceneController.waveStarted && spawning)
        {
            StopAllCoroutines();
            spawning = false;
        }
    }

    #region Basic Zombies

    private IEnumerator SpawnBasicZombies()
    {
        //Set to 0 for unlimited spawning
        if (totalEnemiesToSpawn == 0)
        {
            while (totalEnemiesToSpawn == 0 && sceneController.waveStarted)
            {
                //Check there are spawn points and enemies
                if (spawnPoints.Length > 0 && basicZombies.Length > 0)
                {
                    //If there are spawn point get random number
                    int randomNum = Random.Range(0, spawnPoints.Length - 1);

                    //Select random spawn point and get script
                    GameObject spawnPoint = spawnPoints[randomNum];
                    SpawnController spawnController = spawnPoint.GetComponent<SpawnController>();

                    //Make sure not already spawning
                    if (!spawnController.isSpawning)
                    {
                        //Now spawning
                        spawnController.isSpawning = true;

                        //Get spawn position
                        Vector3 spawnPos = spawnPoint.transform.position;

                        //Get random number
                        randomNum = Random.Range(0, basicZombies.Length - 1);

                        //Get random emeny and script to find spawn rate
                        GameObject zombie = op_BasicZombies.GetPooledObject();
                        EnemyManager enemyManager = zombie.GetComponent<EnemyManager>();
                        float spawnRate = enemyManager.spawnRate;
                        int numTospawn = enemyManager.numberToSpawn;

                        if (enemyManager.spawnDelay > 0 && firstSpawn_BasicZombies)
                            yield return new WaitForSeconds(enemyManager.spawnDelay);

                        //After wait check  if can spawn
                        if (currentEnemiesSpawned >= totalEnemiesToSpawn)
                            continue;

                        //spawn the number to spawn
                        for (int i = 0; i < numTospawn; i++)
                        {
                            //Spawn enemy at random point
                            zombie.transform.position = spawnPos;
                            zombie.transform.rotation = Quaternion.identity;
                            enemyManager.currentHealth = enemyManager.maxHealth;
                            zombie.SetActive(true);
                            enemyManager.StartUpdatePathCoroutine();
                            currentEnemiesSpawned++;
                            firstSpawn_BasicZombies = false;

                            if (i++ < numTospawn)
                            {
                                zombie = op_BasicZombies.GetPooledObject();
                                enemyManager = zombie.GetComponent<EnemyManager>();
                            }
                        }

                        //Stop spawning
                        spawnController.isSpawning = false;

                        //wait for spawn rate
                        yield return new WaitForSeconds(spawnRate);
                    }
                }
            }
        }
        //While we can spawn enemies
        else if (totalEnemiesToSpawn > 0)
        {
            while (currentEnemiesSpawned < totalEnemiesToSpawn && sceneController.waveStarted)
            {
                //Check there are spawn points and enemies
                if (spawnPoints.Length > 0 && basicZombies.Length > 0)
                {
                    //If there are spawn point get random number
                    int randomNum = Random.Range(0, spawnPoints.Length - 1);

                    //Select random spawn point and get script
                    GameObject spawnPoint = spawnPoints[randomNum];
                    SpawnController spawnController = spawnPoint.GetComponent<SpawnController>();

                    //Make sure not already spawning
                    if (!spawnController.isSpawning)
                    {
                        //Now spawning
                        spawnController.isSpawning = true;

                        //Get spawn position
                        Vector3 spawnPos = spawnPoint.transform.position;

                        //Get random number
                        randomNum = Random.Range(0, basicZombies.Length - 1);

                        //Get random emeny and script to find spawn rate
                        GameObject zombie = op_BasicZombies.GetPooledObject();
                        EnemyManager enemyManager = zombie.GetComponent<EnemyManager>();
                        float spawnRate = enemyManager.spawnRate;
                        int numTospawn = enemyManager.numberToSpawn;

                        if (enemyManager.spawnDelay > 0 && firstSpawn_BasicZombies)
                            yield return new WaitForSeconds(enemyManager.spawnDelay);

                        //After wait check  if can spawn
                        if (currentEnemiesSpawned >= totalEnemiesToSpawn)
                            continue;

                        //spawn the number to spawn
                        for (int i = 0; i < numTospawn; i++)
                        {
                            //Spawn enemy at random point
                            zombie.transform.position = spawnPos;
                            zombie.transform.rotation = Quaternion.identity;
                            enemyManager.currentHealth = enemyManager.maxHealth;
                            zombie.SetActive(true);
                            enemyManager.StartUpdatePathCoroutine();
                            currentEnemiesSpawned++;
                            firstSpawn_BasicZombies = false;

                            if (i++ < numTospawn)
                            {
                                zombie = op_BasicZombies.GetPooledObject();
                                enemyManager = zombie.GetComponent<EnemyManager>();
                            }
                        }

                        //Stop spawning
                        spawnController.isSpawning = false;

                        //wait for spawn rate
                        yield return new WaitForSeconds(spawnRate);
                    }
                }
            }
        }

        yield return null;
    }

    #endregion

    #region Fast Zombies

    private IEnumerator SpawnFastZombies()
    {
        //Set to 0 for unlimited spawning
        if (totalEnemiesToSpawn == 0)
        {
            while (totalEnemiesToSpawn == 0)
            {
                //Check there are spawn points and enemies
                if (spawnPoints.Length > 0 && fastZombies.Length > 0)
                {
                    //If there are spawn point get random number
                    int randomNum = Random.Range(0, spawnPoints.Length - 1);

                    //Select random spawn point and get script
                    GameObject spawnPoint = spawnPoints[randomNum];
                    SpawnController spawnController = spawnPoint.GetComponent<SpawnController>();

                    //Make sure not already spawning
                    if (!spawnController.isSpawning)
                    {
                        //Now spawning
                        spawnController.isSpawning = true;

                        //Get spawn position
                        Vector3 spawnPos = spawnPoint.transform.position;

                        //Get random number
                        randomNum = Random.Range(0, fastZombies.Length - 1);

                        //Get random emeny and script to find spawn rate
                        GameObject zombie = op_FastZombies.GetPooledObject();
                        EnemyManager enemyManager = zombie.GetComponent<EnemyManager>();
                        float spawnRate = enemyManager.spawnRate;
                        int numTospawn = enemyManager.numberToSpawn;

                        if (enemyManager.spawnDelay > 0 && firstSpawn_FastZombies)
                            yield return new WaitForSeconds(enemyManager.spawnDelay);

                        //After wait check  if can spawn
                        if (currentEnemiesSpawned >= totalEnemiesToSpawn)
                            continue;

                        //spawn the number to spawn
                        for (int i = 0; i < numTospawn; i++)
                        {
                            //Spawn enemy at random point
                            zombie.transform.position = spawnPos;
                            zombie.transform.rotation = Quaternion.identity;
                            enemyManager.currentHealth = enemyManager.maxHealth;
                            zombie.SetActive(true);
                            enemyManager.StartUpdatePathCoroutine();
                            currentEnemiesSpawned++;
                            firstSpawn_FastZombies = false;

                            if (i++ < numTospawn)
                            {
                                zombie = op_FastZombies.GetPooledObject();
                                enemyManager = zombie.GetComponent<EnemyManager>();
                            }
                        }

                        //Stop spawning
                        spawnController.isSpawning = false;

                        //wait for spawn rate
                        yield return new WaitForSeconds(spawnRate);
                    }
                }
            }
        }
        //While we can spawn enemies
        else if (totalEnemiesToSpawn > 0)
        {
            while (currentEnemiesSpawned < totalEnemiesToSpawn)
            {
                //Check there are spawn points and enemies
                if (spawnPoints.Length > 0 && fastZombies.Length > 0)
                {
                    //If there are spawn point get random number
                    int randomNum = Random.Range(0, spawnPoints.Length - 1);

                    //Select random spawn point and get script
                    GameObject spawnPoint = spawnPoints[randomNum];
                    SpawnController spawnController = spawnPoint.GetComponent<SpawnController>();

                    //Make sure not already spawning
                    if (!spawnController.isSpawning)
                    {
                        //Now spawning
                        spawnController.isSpawning = true;

                        //Get spawn position
                        Vector3 spawnPos = spawnPoint.transform.position;

                        //Get random number
                        randomNum = Random.Range(0, fastZombies.Length - 1);

                        //Get random emeny and script to find spawn rate
                        GameObject zombie = op_FastZombies.GetPooledObject();
                        EnemyManager enemyManager = zombie.GetComponent<EnemyManager>();
                        float spawnRate = enemyManager.spawnRate;
                        int numTospawn = enemyManager.numberToSpawn;

                        if (enemyManager.spawnDelay > 0 && firstSpawn_FastZombies)
                            yield return new WaitForSeconds(enemyManager.spawnDelay);

                        //After wait check  if can spawn
                        if (currentEnemiesSpawned >= totalEnemiesToSpawn)
                            continue;

                        //spawn the number to spawn
                        for (int i = 0; i < numTospawn; i++)
                        {
                            //Spawn enemy at random point
                            zombie.transform.position = spawnPos;
                            zombie.transform.rotation = Quaternion.identity;
                            enemyManager.currentHealth = enemyManager.maxHealth;
                            zombie.SetActive(true);
                            enemyManager.StartUpdatePathCoroutine();
                            currentEnemiesSpawned++;
                            firstSpawn_FastZombies = false;

                            if (i++ < numTospawn)
                            {
                                zombie = op_FastZombies.GetPooledObject();
                                enemyManager = zombie.GetComponent<EnemyManager>();
                            }
                        }

                        //Stop spawning
                        spawnController.isSpawning = false;

                        //wait for spawn rate
                        yield return new WaitForSeconds(spawnRate);
                    }
                }
            }
        }

        yield return null;
    }

    #endregion

    #region Fat Zombies

    private IEnumerator SpawnFatZombies()
    {
        //Set to 0 for unlimited spawning
        if (totalEnemiesToSpawn == 0)
        {
            while (totalEnemiesToSpawn == 0)
            {
                //Check there are spawn points and enemies
                if (spawnPoints.Length > 0 && fatZombies.Length > 0)
                {
                    //If there are spawn point get random number
                    int randomNum = Random.Range(0, spawnPoints.Length - 1);

                    //Select random spawn point and get script
                    GameObject spawnPoint = spawnPoints[randomNum];
                    SpawnController spawnController = spawnPoint.GetComponent<SpawnController>();

                    //Make sure not already spawning
                    if (!spawnController.isSpawning)
                    {
                        //Now spawning
                        spawnController.isSpawning = true;

                        //Get spawn position
                        Vector3 spawnPos = spawnPoint.transform.position;

                        //Get random number
                        randomNum = Random.Range(0, fatZombies.Length - 1);

                        //Get random emeny and script to find spawn rate
                        GameObject zombie = op_FatZombies.GetPooledObject();
                        EnemyManager enemyManager = zombie.GetComponent<EnemyManager>();
                        float spawnRate = enemyManager.spawnRate;
                        int numTospawn = enemyManager.numberToSpawn;

                        if (enemyManager.spawnDelay > 0 && firstSpawn_FatZombies)
                            yield return new WaitForSeconds(enemyManager.spawnDelay);

                        //After wait check  if can spawn
                        if (currentEnemiesSpawned >= totalEnemiesToSpawn)
                            continue;

                        //spawn the number to spawn
                        for (int i = 0; i < numTospawn; i++)
                        {
                            //Spawn enemy at random point
                            zombie.transform.position = spawnPos;
                            zombie.transform.rotation = Quaternion.identity;
                            enemyManager.currentHealth = enemyManager.maxHealth;
                            zombie.SetActive(true);
                            enemyManager.StartUpdatePathCoroutine();
                            currentEnemiesSpawned++;
                            firstSpawn_FatZombies = false;

                            if (i++ < numTospawn)
                            {
                                zombie = op_FatZombies.GetPooledObject();
                                enemyManager = zombie.GetComponent<EnemyManager>();
                            }
                        }

                        //Stop spawning
                        spawnController.isSpawning = false;

                        //wait for spawn rate
                        yield return new WaitForSeconds(spawnRate);
                    }
                }
            }
        }
        //While we can spawn enemies
        else if (totalEnemiesToSpawn > 0)
        {
            while (currentEnemiesSpawned < totalEnemiesToSpawn)
            {
                //Check there are spawn points and enemies
                if (spawnPoints.Length > 0 && fatZombies.Length > 0)
                {
                    //If there are spawn point get random number
                    int randomNum = Random.Range(0, spawnPoints.Length - 1);

                    //Select random spawn point and get script
                    GameObject spawnPoint = spawnPoints[randomNum];
                    SpawnController spawnController = spawnPoint.GetComponent<SpawnController>();

                    //Make sure not already spawning
                    if (!spawnController.isSpawning)
                    {
                        //Now spawning
                        spawnController.isSpawning = true;

                        //Get spawn position
                        Vector3 spawnPos = spawnPoint.transform.position;

                        //Get random number
                        randomNum = Random.Range(0, fatZombies.Length - 1);

                        //Get random emeny and script to find spawn rate
                        GameObject zombie = op_FatZombies.GetPooledObject();
                        EnemyManager enemyManager = zombie.GetComponent<EnemyManager>();
                        float spawnRate = enemyManager.spawnRate;
                        int numTospawn = enemyManager.numberToSpawn;

                        if (enemyManager.spawnDelay > 0 && firstSpawn_FatZombies)
                            yield return new WaitForSeconds(enemyManager.spawnDelay);

                        //After wait check  if can spawn
                        if (currentEnemiesSpawned >= totalEnemiesToSpawn)
                            continue;

                        //spawn the number to spawn
                        for (int i = 0; i < numTospawn; i++)
                        {
                            //Spawn enemy at random point
                            zombie.transform.position = spawnPos;
                            zombie.transform.rotation = Quaternion.identity;
                            enemyManager.currentHealth = enemyManager.maxHealth;
                            zombie.SetActive(true);
                            enemyManager.StartUpdatePathCoroutine();
                            currentEnemiesSpawned++;
                            firstSpawn_FatZombies = false;

                            if (i++ < numTospawn)
                            {
                                zombie = op_FatZombies.GetPooledObject();
                                enemyManager = zombie.GetComponent<EnemyManager>();
                            }
                        }

                        //Stop spawning
                        spawnController.isSpawning = false;

                        //wait for spawn rate
                        yield return new WaitForSeconds(spawnRate);
                    }
                }
            }
        }

        yield return null;
    }

    #endregion
}
