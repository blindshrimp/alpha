﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour
{
    public float wallHealth;
    public bool isDestoryed;

    private void Update()
    {
        if (wallHealth <= 0)
        {
            isDestoryed = true;
            gameObject.SetActive(false);
        }
        else
            isDestoryed = false;
    }
}
