﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponController : MonoBehaviour
{
    [Header("Weapons")]    
    public GameObject[] weapons;
    public int currentWeapon;
    //public GameObject equiptedWeapon;

    [Header("Fire Point")]
    public Vector2 firePointLocation;
    public GameObject firePoint;

    private SpriteRenderer spriteRenderer;
    private BoxCollider2D bc2D;
    private GameObject equiptedWeapon;
    private GunController equiptedWeaponController;

	// Use this for initialization
	void Start ()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        bc2D = GetComponent<BoxCollider2D>();
        currentWeapon = 0;
        ChangeWeapon(currentWeapon);
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Shoot weapon
        if (Time.time > equiptedWeaponController.nextFire && equiptedWeaponController.currentAmmo > 0)
        {
            if (!equiptedWeaponController.isAutomaticWeapon)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    //firePointLocation = new Vector2(firePoint.transform.position.x, firePoint.transform.position.y);
                    //equiptedWeaponController.Shoot(firePointLocation);
                    equiptedWeaponController.Shoot(firePoint.transform.position);
                }
            }
            else
            {
                if (Input.GetMouseButton(0) && equiptedWeaponController.isAutomaticWeapon)
                {
                    //firePointLocation = new Vector2(firePoint.transform.position.x, firePoint.transform.position.y);
                    //equiptedWeaponController.Shoot(firePointLocation);
                    equiptedWeaponController.Shoot(firePoint.transform.position);
                }
            }
        }

        //Reload weapon
        if (Input.GetKeyDown(KeyCode.R) && equiptedWeaponController.currentAmmo <equiptedWeaponController.maxAmmo)
            equiptedWeaponController.currentAmmo = equiptedWeaponController.maxAmmo;

        ////Auto reload weapon
        //if (equiptedWeaponController.currentAmmo <= 0)
        //    equiptedWeaponController.currentAmmo = equiptedWeaponController.maxAmmo;

        //Change weapon
        if (Input.GetKeyDown(KeyCode.Alpha1))        
            ChangeWeapon(0);            

        if (Input.GetKeyDown(KeyCode.Alpha2))        
            ChangeWeapon(1);
    }

    private void ChangeWeapon(int num)
    {
        for (int i = 0;  i < weapons.Length; i++)
        {
            GameObject weapon = weapons[i];

            if (transform.FindChild(weapon.name + "(Clone)") == null)
                weapon = Instantiate(weapon, transform);
            else
                weapon = transform.FindChild(weapon.name + "(Clone)").gameObject;

            GunController gunController = weapon.GetComponent<GunController>();

            if (i == num)
            {                
                weapon.SetActive(true);

                Sprite playerSprite = gunController.playerSprite;
                spriteRenderer.sprite = playerSprite;

                firePoint.transform.localPosition = gunController.firePointLocation;
                firePointLocation = new Vector2(firePoint.transform.position.x, firePoint.transform.position.y); 
                                               
                bc2D.size = gunController.boxColliderSize;

                gunController.isEquipted = true;
                gunController.nextFire = 0;

                equiptedWeapon = weapon;
                equiptedWeaponController = gunController;
            }
            else
            {
                gunController.isEquipted = false;
                weapon.SetActive(false);               
            }
        }

        currentWeapon = num;
    }
}
