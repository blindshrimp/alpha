﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]

public class EnemyAI : MonoBehaviour
{
    //What to chase
    public Transform target;

    //How many times a second to update path
    public float updateRate = 2f;
    
    //The calculated  path
    public Path path;

    //The Ai's speed per second
    public float speed = 300f;
    public ForceMode2D fMode;

    //[HideInInspector]
    public bool pathHasEnded = false;

    //Max distance from AI  to  waypoint  for it to  continue to next waypoint
    public float nextWaypointDistance = 3f;

    //The waypoint we are currently moving towards
    private int currentWaypoint = 0;

    //Caching
    private Seeker seeker;
    private Rigidbody2D rb2D;

    private void Start()
    {
        seeker = GetComponent<Seeker>();
        rb2D = GetComponent<Rigidbody2D>();

        if (target == null)
        {
            Debug.Log("No Target!!!");
            return;
        }

        ////Start a new path and return path to OnPathcomplete
        //seeker.StartPath(transform.position, target.position, OnPathComplete);

        StartCoroutine("UpdatePath");
    }

    private void FixedUpdate()
    {
        if (target == null)
        {
            //Search for  player
            return;
        }

        //Look at player

        if (path == null)
        {
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            if (pathHasEnded)
                return;

            Debug.Log("Path has ended.");
            pathHasEnded = true;
            return;
        }

        pathHasEnded = false;

        //Direction to next waypoint
        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        dir *= speed * Time.fixedDeltaTime;

        //Move the AI
        rb2D.AddForce(dir, fMode);

        if  (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }
    }

    public void OnPathComplete(Path p)
    {
        Debug.Log("Found Path, Error: " + p.error);

        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    private IEnumerator UpdatePath()
    {
        if (target == null)
        {
            //TODO: Insert a player search
            yield return null;
        }

        //Start a new path and return path to OnPathcomplete
        seeker.StartPath(transform.position, target.position, OnPathComplete);

        yield return new WaitForSeconds(1/updateRate);

        StartCoroutine("UpdatePath");
    }
}
