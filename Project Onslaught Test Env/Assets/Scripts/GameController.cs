﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class GameController : MonoBehaviour
{
    public static GameController gameControl;
    //public string saveFileName;

    #region Game Settings

    [Header("Game Settings")]
    public bool autoLoadSettings;
    public bool showErrorLog;//Add the rest
    public bool isDevMode;

    #endregion

    #region Player Settings

    [Header("Player Settings")]
    public int playerHealth;
    public float playerSpeed;
    public bool isInvincible;
    public int playerJunk;

    #endregion

    #region Enemy Settings

    #region Zombie Settings

    [Header("Zombie Settings")]
    public float zombieHealth;
    public float zombieSpeed;
    public int zombieDamage;
    public float zombieAttackRate;
    public int zombieJunkVal;
    public float zombieSpawnRate;
    public int zombieInitSpawnCount;
    public float zombieSpawnDelay;
    public bool zombieUseSpawnDelay;
    public bool zombieIsUnlocked;

    #endregion

    #region Fast Zombie Settings

    [Header("Fast Zombie Settings")]
    public float fastZombieHealth;
    public float fastZombieSpeed;
    public int fastZombieDamage;
    public float fastZombieAttackRate;
    public int fastZombieJunkVal;
    public float fastZombieSpawnRate;
    public int fastZombieInitSpawnCount;
    public float fastZombieSpawnDelay;
    public bool fastZombieUseSpawnDelay;
    public bool fastZombieIsUnlocked;

    #endregion

    #region Fat Zombie Settings

    [Header("Fat Zombie Settings")]
    public float fatZombieHealth;
    public float fatZombieSpeed;
    public int fatZombieDamage;
    public float fatZombieAttackRate;
    public int fatZombieJunkVal;
    public float fatZombieSpawnRate;
    public int fatZombieInitSpawnCount;
    public float fatZombieSpawnDelay;
    public bool fatZombieUseSpawnDelay;
    public bool fatZombieIsUnlocked;

    #endregion

    #endregion

    #region Weapon Settings

    #region Pistol Settings

    public float bulletSpeed;

    [Header("Pistol Settings")]
    public int pistolDamage;
    public int pistolMaxAmmo;
    public float pistolRange;
    public float pistolFireRate;
    public float pistolReloadTime;
    public bool pistolIsAutomatic;
    public bool pistolIsUnlocked;

    #endregion

    #region Machine Gun Settings

    [Header("Machine Gun Settings")]
    public int machineGunDamage;
    public int machineGunMaxAmmo;
    public float machineGunRange;
    public float machineGunFireRate;
    public float machineGunReloadTime;
    public bool machineGunIsAutomatic;
    public bool machineGunIsUnlocked;

    #endregion

    #region Shotgun Settings

    [Header("Shotgun Settings")]
    public int shotgunDamage;
    public int shotgunMaxAmmo;
    public float shotgunRange;
    public float shotgunFireRate;
    public float shotgunReloadTime;
    public bool shotgunIsAutomatic;
    public bool shotgunIsUnlocked;

    #endregion

    #endregion

    #region Trap Settings

    #region Trap 1 Settings

    [Header("Trap 1 Settings")]//Add these
    public float trap1Range;
    public float trap1AttackRate;
    public int trap1Damage;
    public int trap1CostToFix;
    public int trap1AmountFixed;
    public bool trap1IsUnlocked;

    #endregion

    #endregion

    #region Wave Settings

    [Header("Wave Settings")]
    public int initTotalToSpawn;
    public int startingWave;

    #endregion

    #region Wall Settings

    [Header("Wall Settings")]
    public int wallHealth;
    public int wallCostToFix;
    public int wallAmountFixed;

    #endregion

    private void Awake()
    {
        if (gameControl == null)
        {
            DontDestroyOnLoad(gameObject);
            gameControl = this;
        }
        else if (gameControl != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        //TODO: set default settings

        //Auto load settings
        if (autoLoadSettings)
            Load();
    }

    public void Save()
    {             
        CreateGameSettings();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/gameSettings.dat"))        
            LoadGameSettings();        
    }

    private void CreateGameSettings()
    {
        //TODO: handle multiple save files

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gameSettings.dat");
        GameSettings settings = new GameSettings();

        #region Set Settings

        //Set player settings
        settings.playerHealth = playerHealth;
        settings.playerSpeed = playerSpeed;
        settings.isInvincible = isInvincible;
        settings.playerJunk = playerJunk;

        #region Set Enemy Settings

        //Set zombie settings
        settings.zombieHealth = zombieHealth;
        settings.zombieSpeed = zombieSpeed;
        settings.zombieDamage = zombieDamage;
        settings.zombieAttackRate = zombieAttackRate;
        settings.zombieJunkVal = zombieJunkVal;
        settings.zombieSpawnRate = zombieSpawnRate;
        settings.zombieInitSpawnCount = zombieInitSpawnCount;
        settings.zombieSpawnDelay = zombieSpawnDelay;
        settings.zombieUseSpawnDelay = zombieUseSpawnDelay;
        settings.zombieIsUnlocked = zombieIsUnlocked;

        //Set fast zombie settings
        settings.fastZombieHealth = fastZombieHealth;
        settings.fastZombieSpeed = fastZombieSpeed;
        settings.fastZombieDamage = fastZombieDamage;
        settings.fastZombieAttackRate = fastZombieAttackRate;
        settings.fastZombieJunkVal = fastZombieJunkVal;
        settings.fastZombieSpawnRate = fastZombieSpawnRate;
        settings.fastZombieInitSpawnCount = fastZombieInitSpawnCount;
        settings.fastZombieSpawnDelay = fastZombieSpawnDelay;
        settings.fastZombieUseSpawnDelay = fastZombieUseSpawnDelay;
        settings.fastZombieIsUnlocked = fastZombieIsUnlocked;

        //Set fat zombie settings
        settings.fatZombieHealth = fatZombieHealth;
        settings.fatZombieSpeed = fatZombieSpeed;
        settings.fatZombieDamage = fatZombieDamage;
        settings.fatZombieAttackRate = fatZombieAttackRate;
        settings.fatZombieJunkVal = fatZombieJunkVal;
        settings.fatZombieSpawnRate = fatZombieSpawnRate;
        settings.fatZombieInitSpawnCount = fatZombieInitSpawnCount;
        settings.fatZombieSpawnDelay = fatZombieSpawnDelay;
        settings.fatZombieUseSpawnDelay = fatZombieUseSpawnDelay;
        settings.fatZombieIsUnlocked = fatZombieIsUnlocked;

        #endregion

        #region Set Weapon Settings

        //Set pistol settings
        settings.pistolDamage = pistolDamage;
        settings.pistolMaxAmmo = pistolMaxAmmo;
        settings.pistolRange = pistolRange;
        settings.pistolFireRate = pistolFireRate;
        settings.pistolReloadTime = pistolReloadTime;
        settings.pistolIsAutomatic = pistolIsAutomatic;
        settings.pistolIsUnlocked = pistolIsUnlocked;

        //Set machine gun settings
        settings.machineGunDamage = machineGunDamage;
        settings.machineGunMaxAmmo = machineGunMaxAmmo;
        settings.machineGunRange = machineGunRange;
        settings.machineGunFireRate = machineGunFireRate;
        settings.machineGunReloadTime = machineGunReloadTime;
        settings.machineGunIsAutomatic = machineGunIsAutomatic;
        settings.machineGunIsUnlocked = machineGunIsUnlocked;

        //Set shotgun settings
        settings.shotgunDamage = shotgunDamage;
        settings.shotgunMaxAmmo = shotgunMaxAmmo;
        settings.shotgunRange = shotgunRange;
        settings.shotgunFireRate = shotgunFireRate;
        settings.shotgunReloadTime = shotgunReloadTime;
        settings.shotgunIsAutomatic = shotgunIsAutomatic;
        settings.shotgunIsUnlocked = shotgunIsUnlocked;

        #endregion

        //TODO: Set trap settings

        //Set wave settings
        settings.initTotalToSpawn = initTotalToSpawn;
        settings.startingWave = startingWave;

        //Set wall settings
        settings.wallHealth = wallHealth;
        settings.wallCostToFix = wallCostToFix;
        settings.wallAmountFixed = wallAmountFixed;

        #endregion

        bf.Serialize(file, settings);
        file.Close();
    }

    //private void UpdateSettings()
    //{
    //    if (File.Exists(Application.persistentDataPath + "/gameSettings.dat"))
    //    {
    //        BinaryFormatter bf = new BinaryFormatter();
    //        FileStream file = File.Open(Application.persistentDataPath + "/gameSettings.dat", FileMode.Open);
    //        GameSettings settings = (GameSettings)bf.Deserialize(file);            
            
    //        if (playerHealth != settings.playerHealth)
    //            settings.playerHealth = playerHealth;
    //    }
    //}

    private void LoadGameSettings()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/gameSettings.dat", FileMode.Open);
        GameSettings settings = (GameSettings)bf.Deserialize(file);
        file.Close();

        #region Set Settings

        //Set player settings
        playerHealth = settings.playerHealth;
        playerSpeed = settings.playerSpeed;
        isInvincible = settings.isInvincible;
        playerJunk = settings.playerJunk;

        #region Set Enemy Settings

        //Set zombie settings
        zombieHealth = settings.zombieHealth;
        zombieSpeed = settings.zombieSpeed;
        zombieDamage = settings.zombieDamage;
        zombieAttackRate = settings.zombieAttackRate;
        zombieJunkVal = settings.zombieJunkVal;
        zombieSpawnRate = settings.zombieSpawnRate;
        zombieInitSpawnCount = settings.zombieInitSpawnCount;
        zombieSpawnDelay = settings.zombieSpawnDelay;
        zombieUseSpawnDelay = settings.zombieUseSpawnDelay;
        zombieIsUnlocked = settings.zombieIsUnlocked;

        //Set fast zombie settings
        fastZombieHealth = settings.fastZombieHealth;
        fastZombieSpeed = settings.fastZombieSpeed;
        fastZombieDamage = settings.fastZombieDamage;
        fastZombieAttackRate = settings.fastZombieAttackRate;
        fastZombieJunkVal = settings.fastZombieJunkVal;
        fastZombieSpawnRate = settings.fastZombieSpawnRate;
        fastZombieInitSpawnCount = settings.fastZombieInitSpawnCount;
        fastZombieSpawnDelay = settings.fastZombieSpawnDelay;
        fastZombieUseSpawnDelay = settings.fastZombieUseSpawnDelay;
        fastZombieIsUnlocked = settings.fastZombieIsUnlocked;

        //Set fat zombie settings
        fatZombieHealth = settings.fatZombieHealth;
        fatZombieSpeed = settings.fatZombieSpeed;
        fatZombieDamage = settings.fatZombieDamage;
        fatZombieAttackRate = settings.fatZombieAttackRate;
        fatZombieJunkVal = settings.fatZombieJunkVal;
        fatZombieSpawnRate = settings.fatZombieSpawnRate;
        fatZombieInitSpawnCount = settings.fatZombieInitSpawnCount;
        fatZombieSpawnDelay = settings.fatZombieSpawnDelay;
        fatZombieUseSpawnDelay = settings.fatZombieUseSpawnDelay;
        fatZombieIsUnlocked = settings.fatZombieIsUnlocked;

        #endregion

        #region Set Weapon Settings

        //Set pistol settings
        pistolDamage = settings.pistolDamage;
        pistolMaxAmmo = settings.pistolMaxAmmo;
        pistolRange = settings.pistolRange;
        pistolFireRate = settings.pistolFireRate;
        pistolReloadTime = settings.pistolReloadTime;
        pistolIsAutomatic = settings.pistolIsAutomatic;
        pistolIsUnlocked = settings.pistolIsUnlocked;

        //Set machine gun settings
        machineGunDamage = settings.machineGunDamage;
        machineGunMaxAmmo = settings.machineGunMaxAmmo;
        machineGunRange = settings.machineGunRange;
        machineGunFireRate = settings.machineGunFireRate;
        machineGunReloadTime = settings.machineGunReloadTime;
        machineGunIsAutomatic = settings.machineGunIsAutomatic;
        machineGunIsUnlocked = settings.machineGunIsUnlocked;

        //Set shotgun settings
        shotgunDamage = settings.shotgunDamage;
        shotgunMaxAmmo = settings.shotgunMaxAmmo;
        shotgunRange = settings.shotgunRange;
        shotgunFireRate = settings.shotgunFireRate;
        shotgunReloadTime = settings.shotgunReloadTime;
        shotgunIsAutomatic = settings.shotgunIsAutomatic;
        shotgunIsUnlocked = settings.shotgunIsUnlocked;

        #endregion

        //TODO: Set trap settings

        //Set wave settings
        initTotalToSpawn = settings.initTotalToSpawn;
        startingWave = settings.startingWave;

        //Set wall settings
        wallHealth = settings.wallHealth;
        wallCostToFix = settings.wallCostToFix;
        wallAmountFixed = settings.wallAmountFixed;

        #endregion
    }
}

[Serializable]
class GameSettings
{
    #region Game Settings

    [Header("Game Settings")]
    public bool isDevMode;

    #endregion

    #region Player Settings

    [Header("Player Settings")]
    public int playerHealth;
    public float playerSpeed;
    public bool isInvincible;
    public int playerJunk;

    #endregion

    #region Enemy Settings

    #region Zombie Settings

    [Header("Zombie Settings")]
    public float zombieHealth;
    public float zombieSpeed;
    public int zombieDamage;
    public float zombieAttackRate;
    public int zombieJunkVal;
    public float zombieSpawnRate;
    public int zombieInitSpawnCount;
    public float zombieSpawnDelay;
    public bool zombieUseSpawnDelay;
    public bool zombieIsUnlocked;

    #endregion

    #region Fast Zombie Settings

    [Header("Fast Zombie Settings")]
    public float fastZombieHealth;
    public float fastZombieSpeed;
    public int fastZombieDamage;
    public float fastZombieAttackRate;
    public int fastZombieJunkVal;
    public float fastZombieSpawnRate;
    public int fastZombieInitSpawnCount;
    public float fastZombieSpawnDelay;
    public bool fastZombieUseSpawnDelay;
    public bool fastZombieIsUnlocked;

    #endregion

    #region Fat Zombie Settings

    [Header("Fat Zombie Settings")]
    public float fatZombieHealth;
    public float fatZombieSpeed;
    public int fatZombieDamage;
    public float fatZombieAttackRate;
    public int fatZombieJunkVal;
    public float fatZombieSpawnRate;
    public int fatZombieInitSpawnCount;
    public float fatZombieSpawnDelay;
    public bool fatZombieUseSpawnDelay;
    public bool fatZombieIsUnlocked;

    #endregion

    #endregion

    #region Weapon Settings

    #region Pistol Settings

    public float bulletSpeed;

    [Header("Pistol Settings")]
    public int pistolDamage;
    public int pistolMaxAmmo;
    public float pistolRange;
    public float pistolFireRate;
    public float pistolReloadTime;
    public bool pistolIsAutomatic;
    public bool pistolIsUnlocked;

    #endregion

    #region Machine Gun Settings

    [Header("Machine Gun Settings")]
    public int machineGunDamage;
    public int machineGunMaxAmmo;
    public float machineGunRange;
    public float machineGunFireRate;
    public float machineGunReloadTime;
    public bool machineGunIsAutomatic;
    public bool machineGunIsUnlocked;

    #endregion

    #region Shotgun Settings

    [Header("Shotgun Settings")]
    public int shotgunDamage;
    public int shotgunMaxAmmo;
    public float shotgunRange;
    public float shotgunFireRate;
    public float shotgunReloadTime;
    public bool shotgunIsAutomatic;
    public bool shotgunIsUnlocked;

    #endregion

    #endregion

    #region Trap Settings

    #region Trap 1 Settings

    [Header("Trap 1 Settings")]
    public int trap1Damage;
    public int trap1CostToFix;
    public int trap1AmountFixed;

    #endregion

    #endregion

    #region Wave Settings

    [Header("Wave Settings")]
    public int initTotalToSpawn;
    public int startingWave;

    #endregion

    #region Wall Settings

    [Header("Wall Settings")]
    public int wallHealth;
    public int wallCostToFix;
    public int wallAmountFixed;

    #endregion

}
