﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonListControll : MonoBehaviour {

    [SerializeField]
    private GameObject buttonTemplate;

    [SerializeField]
    private Transform contentList;

    

    private void Start()
    {
        for(int i = 1; i <= 10; i++)
        {
            GameObject btn = Instantiate(buttonTemplate);
            btn.transform.SetParent(contentList, false);
            btn.SetActive(true);
            btn.GetComponent<ButtonListButton>().SetText("Zombie " + i);
        }
    }
}
