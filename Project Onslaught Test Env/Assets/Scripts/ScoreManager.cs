﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public int totalJunk { get; private set; }
    public int junkThisWave { get; private set; }

    public Text scoreText;

    private void Update()
    {
        scoreText.text = "Junk: " + totalJunk;
    }

    public void AddJunk(int junk, bool includedInWave)
    {
        totalJunk += junk;
        
        if(includedInWave)
            junkThisWave += junk;
    }

    public void MinusJunk(int junk, bool includedInWave)
    {
        totalJunk -= junk;

        if (includedInWave)
            junkThisWave -= junk;
    }

    public void ResetJunkThisWave()
    {
        junkThisWave = 0;
    }

    public void ResetTotalJunk()
    {
        totalJunk = 0;
    } 


}
