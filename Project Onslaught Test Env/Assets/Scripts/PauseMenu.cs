﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public bool isPaused;

    public GameObject pauseMenuCanvas;

	// Use this for initialization
	void Start ()
    {
        isPaused = false;
        //pauseMenuCanvas = GameObject.Find("Paused Menu");	
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (isPaused)
        {
            pauseMenuCanvas.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            pauseMenuCanvas.SetActive(false);
            Time.timeScale = 1;
        }

        if (Input.GetKeyDown(KeyCode.P))
            isPaused = !isPaused;		
	}
}
