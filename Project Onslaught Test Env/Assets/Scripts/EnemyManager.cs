﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]

public class EnemyManager : MonoBehaviour
{
    [Header("Enemy Stats")]
    public int maxHealth;
    public int currentHealth;
    public float speed;
    public int damage;
    public float attackRate;
    public float chaseSpeed;
    public int junkValue;

    private float nextAttack;
    private Rigidbody2D rb2D;

    [Header("Spawn Settings")]
    public float spawnRate;
    public int numberToSpawn;
    public float spawnDelay;

    [Header("Pathfinding")]
    public Transform target;
    public float updateRate;
    public Path path;
    public ForceMode2D forceMode;
    public bool pathHasEnded;
    public float nextWaypointDistance;

    private int currentWaypoint;
    private GameObject player;
    private Seeker seeker;

    private bool isColliding;
    private bool hasPastWall;
    private GameObject wallPoint;

    private ScoreManager scoreManager;

    //private Vector3 reach;
    //private Vector3 moveDirection;

    //private Vector3 playerPosition;


    // Use this for initialization
    void Start ()
    {
        updateRate = 2f;
        pathHasEnded = false;
        nextWaypointDistance = 3f;
        currentWaypoint = 0;
        isColliding = false;
        currentHealth = maxHealth;

        rb2D = GetComponent<Rigidbody2D>();
        seeker = GetComponent<Seeker>();
        scoreManager = GameObject.Find("Scene Manager").GetComponent<ScoreManager>();

        player = GameObject.Find("Player");
        target = player.transform;
        wallPoint = GameObject.Find("Wall Point");

        //StartCoroutine("UpdatePath");
        

        //moveDirection = new Vector3(transform.position.x + 1, transform.position.y, 0);

        //moveDirection.Normalize();
        //reach = transform.Find("Reach").position;

    }

    // Update is called once per frame
    void Update ()
    {
        //Face Player
        if (player.transform.position.x > transform.position.x)
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 180, transform.rotation.eulerAngles.z);
        else transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0, transform.rotation.eulerAngles.z);

        //Check if passed wall
        if (transform.position.x < wallPoint.transform.position.x)
            hasPastWall = true;

        //destroy when health reches 0
        if (currentHealth <= 0)
        {
            StopAllCoroutines();
            scoreManager.AddJunk(junkValue, true);
            gameObject.SetActive(false);
        }

	}

    private void FixedUpdate()
    {
        if (path == null)
            return;

        if (target == null)
        {
            target = GameObject.Find("Player").transform;
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            pathHasEnded = true;
            return;
        }

        pathHasEnded = false;

        //Direction to  next waypoint
        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        rb2D.AddForce(dir * speed * Time.fixedDeltaTime, forceMode);

        if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }

        //move towards base while not colliding with base  walls
        //if (!isColliding && !hasPastWall)
        //    rb2D.velocity = Vector2.left * speed * Time.fixedDeltaTime;        



        //if (!isColliding && hasPastWall)
        //{
        //    //Get player position
        //    Vector3 direction = (player.transform.position - transform.position).normalized;

        //    //Move towards player
        //    rb2D.MovePosition(transform.position + direction * chaseSpeed * Time.fixedDeltaTime);
        //}

        if (rb2D.IsSleeping())        
            rb2D.WakeUp();
    }

    #region Pathfinding

    public  void OnPathComplete(Path p)
    {
        if(!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    public void StartUpdatePathCoroutine()
    {
        StartCoroutine("UpdatePath");
    }

    public void StopUpdatePathCoroutine()
    {
        StopAllCoroutines();
    }

    private IEnumerator UpdatePath()
    {
        if (target == null)
        {
            target = GameObject.Find("Player").transform;
            yield return null;
        }

        //Start a new path and return path to OnPathcomplete
        seeker.StartPath(transform.position, target.position, OnPathComplete);
        yield return new WaitForSeconds(1 / updateRate);

        StartCoroutine("UpdatePath");
    }

    #endregion

    #region OnCollision

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Player")
            isColliding = true;

        if (collision.gameObject.tag == "Bullet")
            TakeBulletDamage(collision.gameObject);

        if (collision.gameObject.tag == "Melee")
            TakeMeleeDamage(collision.gameObject);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
            DealPlayerDamage(collision.gameObject);

        if (collision.gameObject.tag == "Wall")
        {
            DealWallDamage(collision.gameObject);
        }

        if (collision.gameObject.tag == "Melee")
            TakeMeleeDamage(collision.gameObject);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Player")
            isColliding = false;
    }

    #endregion

    #region OnTrigger

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
            TakeBulletDamage(collision.gameObject);

        if (collision.gameObject.tag == "Melee")
            TakeMeleeDamage(collision.gameObject);
    }

    #endregion

    #region Damage

    private void TakeBulletDamage(GameObject weapon)
    {
        BulletMover bulletMover = weapon.GetComponent<BulletMover>();
        currentHealth -= bulletMover.damage;
        weapon.SetActive(false);
    }

    private void TakeMeleeDamage(GameObject weapon)
    {
        MeleeDamageController meleeDamageController = weapon.GetComponent<MeleeDamageController>();
        if(meleeDamageController.isHitting)
            currentHealth -= meleeDamageController.damage;
    }

    private void DealPlayerDamage(GameObject player)
    {
        if (Time.time > nextAttack)
        {
            PlayerController playerController = player.GetComponent<PlayerController>();
            playerController.health -= damage;

            nextAttack = Time.time + attackRate;
        }
    }

    private void DealWallDamage(GameObject wall)
    {
        isColliding = true;

        if (Time.time > nextAttack)
        {
            WallController wallController = wall.GetComponent<WallController>();
            wallController.wallHealth -= damage;
            nextAttack = Time.time + attackRate;
        }
        isColliding = false;
    }

    #endregion
}
