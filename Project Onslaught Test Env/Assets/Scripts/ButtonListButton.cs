﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonListButton : MonoBehaviour {

    [SerializeField]
    private Text buttonText;

	public void SetText(string text)
    {
        buttonText.text = text;
    }    
}
