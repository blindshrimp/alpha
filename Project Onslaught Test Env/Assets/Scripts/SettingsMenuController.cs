﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SettingsMenuController : MonoBehaviour {

    #region UI

    [Header("Game UI")]
    public Toggle autoLoadToggle;
    public Toggle showLogToggle;

    [Header("Player UI")]
    public TMP_InputField playerHealthText;
    public Toggle isInvincibleToggle;
    public TMP_InputField playerSpeedText;
    public TMP_InputField playerJunkText;

    #region Zombie UI

    [Header("Zombie UI")]
    public TMP_InputField zombieHealthText;
    public TMP_InputField zombieSpeedText;
    public TMP_InputField zombieDamageText;
    public TMP_InputField zombieAttackRateText;
    public TMP_InputField zombieSpawnRateText;
    public TMP_InputField zombieSpawnDelayText;
    public TMP_InputField zombieSpawnCountText;
    public TMP_InputField zombieJunkText;
    public Toggle zombieUseDelayToggle;
    public Toggle zombieIsUnlocked;


    #endregion

    #region Fast Zombie UI

    [Header("Fast Zombie UI")]
    public TMP_InputField fastZombieHealthText;
    public TMP_InputField fastZombieSpeedText;
    public TMP_InputField fastZombieDamageText;
    public TMP_InputField fastZombieAttackRateText;
    public TMP_InputField fastZombieSpawnRateText;
    public TMP_InputField fastZombieSpawnDelayText;
    public TMP_InputField fastZombieSpawnCountText;
    public TMP_InputField fastZombieJunkText;
    public Toggle fastZombieUseDelayToggle;
    public Toggle fastZombieIsUnlocked;

    #endregion

    #region Fat Zombie UI

    [Header("Fat Zombie UI")]
    public TMP_InputField fatZombieHealthText;
    public TMP_InputField fatZombieSpeedText;
    public TMP_InputField fatZombieDamageText;
    public TMP_InputField fatZombieAttackRateText;
    public TMP_InputField fatZombieSpawnRateText;
    public TMP_InputField fatZombieSpawnDelayText;
    public TMP_InputField fatZombieSpawnCountText;
    public TMP_InputField fatZombieJunkText;
    public Toggle fatZombieUseDelayToggle;
    public Toggle fatZombieIsUnlocked;

    #endregion

    #region Pistol UI

    [Header("Pistol UI")]
    public TMP_InputField pistolRangeText;
    public TMP_InputField pistolFireRateText;
    public TMP_InputField pistolDamageText;
    public TMP_InputField pistolMaxAmmoText;
    public TMP_InputField pistolReloadTimeText;
    public Toggle pistolIsUnlockedToggle;
    public Toggle pistolIsAutomaticToggle;

    #endregion

    #region Machine Gun UI

    [Header("Machine Gun UI")]
    public TMP_InputField machineGunRangeText;
    public TMP_InputField machineGunFireRateText;
    public TMP_InputField machineGunDamageText;
    public TMP_InputField machineGunMaxAmmoText;
    public TMP_InputField machineGunReloadTimeText;
    public Toggle machineGunIsUnlockedToggle;
    public Toggle machineGunIsAutomaticToggle;

    #endregion

    #region Shotgun UI

    [Header("Shotgun UI")]
    public TMP_InputField shotgunRangeText;
    public TMP_InputField shotgunFireRateText;
    public TMP_InputField shotgunDamageText;
    public TMP_InputField shotgunMaxAmmoText;
    public TMP_InputField shotgunReloadTimeText;
    public Toggle shotgunIsUnlockedToggle;
    public Toggle shotgunIsAutomaticToggle;

    #endregion

    [Header("Trap UI")]
    public TMP_InputField trap1RangeText;
    public TMP_InputField trap1FireRateText;
    public TMP_InputField trap1DamageText;
    public TMP_InputField trap1RepairCostText;
    public TMP_InputField trap1AmountFixedText;
    public Toggle trap1IsUnlockedToggle;

    [Header("Wave UI")]
    public TMP_InputField waveSpawnCountText;
    public TMP_InputField waveStartingWaveText;

    [Header("Wall UI")]
    public TMP_InputField wallHealthText;
    public TMP_InputField wallRepairCostText;
    public TMP_InputField wallAmountFixedText;

    #endregion

    public void LoadGameSettingsUI()
    {     
        //Set Settings Menu UI Elements
        #region Set UI

        //Game Settings
        autoLoadToggle.isOn = GameController.gameControl.autoLoadSettings;
        showLogToggle.isOn = GameController.gameControl.showErrorLog;

        //Player Settings
        playerHealthText.text = GameController.gameControl.playerHealth.ToString();
        isInvincibleToggle.isOn = GameController.gameControl.isInvincible;
        playerSpeedText.text = GameController.gameControl.playerSpeed.ToString();
        playerJunkText.text = GameController.gameControl.playerJunk.ToString();

        //Enemy Settings
        #region Zombie Settings

        zombieHealthText.text = GameController.gameControl.zombieHealth.ToString();
        zombieSpeedText.text = GameController.gameControl.zombieSpeed.ToString();
        zombieDamageText.text = GameController.gameControl.zombieDamage.ToString();
        zombieAttackRateText.text = GameController.gameControl.zombieAttackRate.ToString();
        zombieSpawnRateText.text = GameController.gameControl.zombieSpawnRate.ToString();
        zombieSpawnDelayText.text = GameController.gameControl.zombieSpawnDelay.ToString();
        zombieSpawnCountText.text = GameController.gameControl.zombieInitSpawnCount.ToString();
        zombieJunkText.text = GameController.gameControl.zombieJunkVal.ToString();
        zombieUseDelayToggle.isOn = GameController.gameControl.zombieUseSpawnDelay;
        zombieIsUnlocked.isOn = GameController.gameControl.zombieIsUnlocked;

        #endregion

        #region Fast Zombie Settings

        fastZombieHealthText.text = GameController.gameControl.fastZombieHealth.ToString();
        fastZombieSpeedText.text = GameController.gameControl.fastZombieSpeed.ToString();
        fastZombieDamageText.text = GameController.gameControl.fastZombieDamage.ToString();
        fastZombieAttackRateText.text = GameController.gameControl.fastZombieAttackRate.ToString();
        fastZombieSpawnRateText.text = GameController.gameControl.fastZombieSpawnRate.ToString();
        fastZombieSpawnDelayText.text = GameController.gameControl.fastZombieSpawnDelay.ToString();
        fastZombieSpawnCountText.text = GameController.gameControl.fastZombieInitSpawnCount.ToString();
        fastZombieJunkText.text = GameController.gameControl.fastZombieJunkVal.ToString();
        fastZombieUseDelayToggle.isOn = GameController.gameControl.fastZombieUseSpawnDelay;
        fastZombieIsUnlocked.isOn = GameController.gameControl.fastZombieIsUnlocked;

        #endregion

        #region Fat Zombie Settings

        fatZombieHealthText.text = GameController.gameControl.fatZombieHealth.ToString();
        fatZombieSpeedText.text = GameController.gameControl.fatZombieSpeed.ToString();
        fatZombieDamageText.text = GameController.gameControl.fatZombieDamage.ToString();
        fatZombieAttackRateText.text = GameController.gameControl.fatZombieAttackRate.ToString();
        fatZombieSpawnRateText.text = GameController.gameControl.fatZombieSpawnRate.ToString();
        fatZombieSpawnDelayText.text = GameController.gameControl.fatZombieSpawnDelay.ToString();
        fatZombieSpawnCountText.text = GameController.gameControl.fatZombieInitSpawnCount.ToString();
        fatZombieJunkText.text = GameController.gameControl.fatZombieJunkVal.ToString();
        fatZombieUseDelayToggle.isOn = GameController.gameControl.fatZombieUseSpawnDelay;
        fatZombieIsUnlocked.isOn = GameController.gameControl.fatZombieIsUnlocked;

        #endregion

        //Weapon Settings
        #region Pistol Settings

        pistolRangeText.text = GameController.gameControl.pistolRange.ToString();
        pistolFireRateText.text = GameController.gameControl.pistolFireRate.ToString();
        pistolDamageText.text = GameController.gameControl.pistolDamage.ToString();
        pistolMaxAmmoText.text = GameController.gameControl.pistolMaxAmmo.ToString();
        pistolReloadTimeText.text = GameController.gameControl.pistolReloadTime.ToString();
        pistolIsUnlockedToggle.isOn = GameController.gameControl.pistolIsUnlocked;
        pistolIsAutomaticToggle.isOn = GameController.gameControl.pistolIsAutomatic;

        #endregion

        #region Machine Gun Settings

        machineGunRangeText.text = GameController.gameControl.machineGunRange.ToString();
        machineGunFireRateText.text = GameController.gameControl.machineGunFireRate.ToString();
        machineGunDamageText.text = GameController.gameControl.machineGunDamage.ToString();
        machineGunMaxAmmoText.text = GameController.gameControl.machineGunMaxAmmo.ToString();
        machineGunReloadTimeText.text = GameController.gameControl.machineGunReloadTime.ToString();
        machineGunIsUnlockedToggle.isOn = GameController.gameControl.machineGunIsUnlocked;
        machineGunIsAutomaticToggle.isOn = GameController.gameControl.machineGunIsAutomatic;

        #endregion

        #region Shotgun Settings

        shotgunRangeText.text = GameController.gameControl.shotgunRange.ToString();
        shotgunFireRateText.text = GameController.gameControl.shotgunFireRate.ToString();
        shotgunDamageText.text = GameController.gameControl.shotgunDamage.ToString();
        shotgunMaxAmmoText.text = GameController.gameControl.shotgunMaxAmmo.ToString();
        shotgunReloadTimeText.text = GameController.gameControl.shotgunReloadTime.ToString();
        shotgunIsUnlockedToggle.isOn = GameController.gameControl.shotgunIsUnlocked;
        shotgunIsAutomaticToggle.isOn = GameController.gameControl.shotgunIsAutomatic;

        #endregion

        //Trap Settings
        #region Trap 1

        trap1RangeText.text = GameController.gameControl.trap1Range.ToString();
        trap1FireRateText.text = GameController.gameControl.trap1AttackRate.ToString();
        trap1DamageText.text = GameController.gameControl.trap1Damage.ToString();
        trap1RepairCostText.text = GameController.gameControl.trap1CostToFix.ToString();
        trap1AmountFixedText.text = GameController.gameControl.trap1AmountFixed.ToString();
        trap1IsUnlockedToggle.isOn = GameController.gameControl.trap1IsUnlocked;

        #endregion

        //Wave Settings
        waveSpawnCountText.text = GameController.gameControl.initTotalToSpawn.ToString();
        waveStartingWaveText.text = GameController.gameControl.startingWave.ToString();

        //Wall Settings
        wallHealthText.text = GameController.gameControl.wallHealth.ToString();
        wallRepairCostText.text = GameController.gameControl.wallCostToFix.ToString();
        wallAmountFixedText.text = GameController.gameControl.wallAmountFixed.ToString();

        #endregion
    }

    public void SaveGameSettingsFromUI()
    {
        //Set Game Settings From UI Elements
        #region Set Settings

        //Game Settings
        GameController.gameControl.autoLoadSettings = autoLoadToggle.isOn;
        GameController.gameControl.showErrorLog = showLogToggle.isOn;

        //Player Settings
        GameController.gameControl.playerHealth = Convert.ToInt32(playerHealthText.text);
        GameController.gameControl.playerSpeed = Convert.ToInt32(playerSpeedText.text);
        GameController.gameControl.playerJunk = Convert.ToInt32(playerJunkText.text);
        GameController.gameControl.isInvincible = isInvincibleToggle.isOn;

        //Enemy Settings
        #region Zombie Settings

        GameController.gameControl.zombieHealth = Convert.ToInt32(zombieHealthText.text);
        GameController.gameControl.zombieSpeed = Convert.ToInt32(zombieSpeedText.text);
        GameController.gameControl.zombieDamage = Convert.ToInt32(zombieDamageText.text);
        GameController.gameControl.zombieAttackRate = Convert.ToInt32(zombieAttackRateText.text);
        GameController.gameControl.zombieJunkVal = Convert.ToInt32(zombieJunkText.text);
        GameController.gameControl.zombieSpawnRate = Convert.ToInt32(zombieSpawnRateText.text);
        GameController.gameControl.zombieInitSpawnCount = Convert.ToInt32(zombieSpawnCountText.text);
        GameController.gameControl.zombieSpawnDelay = Convert.ToInt32(zombieSpawnDelayText.text);
        GameController.gameControl.zombieUseSpawnDelay = zombieUseDelayToggle.isOn;
        GameController.gameControl.zombieIsUnlocked = zombieIsUnlocked.isOn;

        #endregion

        #region Fast Zombie Settings

        GameController.gameControl.fastZombieHealth = Convert.ToInt32(fastZombieHealthText.text);
        GameController.gameControl.fastZombieSpeed = Convert.ToInt32(fastZombieSpeedText.text);
        GameController.gameControl.fastZombieDamage = Convert.ToInt32(fastZombieDamageText.text);
        GameController.gameControl.fastZombieAttackRate = Convert.ToInt32(fastZombieAttackRateText.text);
        GameController.gameControl.fastZombieJunkVal = Convert.ToInt32(fastZombieJunkText.text);
        GameController.gameControl.fastZombieSpawnRate = Convert.ToInt32(fastZombieSpawnRateText.text);
        GameController.gameControl.fastZombieInitSpawnCount = Convert.ToInt32(fastZombieSpawnCountText.text);
        GameController.gameControl.fastZombieSpawnDelay = Convert.ToInt32(fastZombieSpawnDelayText.text);
        GameController.gameControl.fastZombieUseSpawnDelay = fastZombieUseDelayToggle.isOn;
        GameController.gameControl.fastZombieIsUnlocked = fastZombieIsUnlocked.isOn;

        #endregion

        #region Fat Zombie Settings

        GameController.gameControl.fatZombieHealth = Convert.ToInt32(fatZombieHealthText.text);
        GameController.gameControl.fatZombieSpeed = Convert.ToInt32(fatZombieSpeedText.text);
        GameController.gameControl.fatZombieDamage = Convert.ToInt32(fatZombieDamageText.text);
        GameController.gameControl.fatZombieAttackRate = Convert.ToInt32(fatZombieAttackRateText.text);
        GameController.gameControl.fatZombieJunkVal = Convert.ToInt32(fatZombieJunkText.text);
        GameController.gameControl.fatZombieSpawnRate = Convert.ToInt32(fatZombieSpawnRateText.text);
        GameController.gameControl.fatZombieInitSpawnCount = Convert.ToInt32(fatZombieSpawnCountText.text);
        GameController.gameControl.fatZombieSpawnDelay = Convert.ToInt32(fatZombieSpawnDelayText.text);
        GameController.gameControl.fatZombieUseSpawnDelay = fatZombieUseDelayToggle.isOn;
        GameController.gameControl.fatZombieIsUnlocked = fatZombieIsUnlocked.isOn;

        #endregion

        //Weapon Settings
        #region Pistol Settings

        GameController.gameControl.pistolDamage = Convert.ToInt32(pistolDamageText.text);
        GameController.gameControl.pistolMaxAmmo = Convert.ToInt32(pistolMaxAmmoText.text);
        GameController.gameControl.pistolRange = Convert.ToInt32(pistolRangeText.text);
        GameController.gameControl.pistolFireRate = Convert.ToInt32(pistolFireRateText.text);
        GameController.gameControl.pistolReloadTime = Convert.ToInt32(pistolReloadTimeText.text);
        GameController.gameControl.pistolIsAutomatic = pistolIsAutomaticToggle.isOn;
        GameController.gameControl.pistolIsUnlocked = pistolIsUnlockedToggle.isOn;

        #endregion

        #region Machine Gun Settings

        GameController.gameControl.machineGunDamage = Convert.ToInt32(machineGunDamageText.text);
        GameController.gameControl.machineGunMaxAmmo = Convert.ToInt32(machineGunMaxAmmoText.text);
        GameController.gameControl.machineGunRange = Convert.ToInt32(machineGunRangeText.text);
        GameController.gameControl.machineGunFireRate = Convert.ToInt32(machineGunFireRateText.text);
        GameController.gameControl.machineGunReloadTime = Convert.ToInt32(machineGunReloadTimeText.text);
        GameController.gameControl.machineGunIsAutomatic = machineGunIsAutomaticToggle.isOn;
        GameController.gameControl.machineGunIsUnlocked = machineGunIsUnlockedToggle.isOn;

        #endregion

        #region Shotgun Settings

        GameController.gameControl.shotgunDamage = Convert.ToInt32(shotgunDamageText.text);
        GameController.gameControl.shotgunMaxAmmo = Convert.ToInt32(shotgunMaxAmmoText.text);
        GameController.gameControl.shotgunRange = Convert.ToInt32(shotgunRangeText.text);
        GameController.gameControl.shotgunFireRate = Convert.ToInt32(shotgunFireRateText.text);
        GameController.gameControl.shotgunReloadTime = Convert.ToInt32(shotgunReloadTimeText.text);
        GameController.gameControl.shotgunIsAutomatic = shotgunIsAutomaticToggle.isOn;
        GameController.gameControl.shotgunIsUnlocked = shotgunIsUnlockedToggle.isOn;

        #endregion

        //Trap Settings
        #region Trap 1 Settings

        GameController.gameControl.trap1Range = Convert.ToInt32(trap1RangeText.text);
        GameController.gameControl.trap1AttackRate = Convert.ToInt32(trap1FireRateText.text);
        GameController.gameControl.trap1Damage = Convert.ToInt32(trap1DamageText.text);
        GameController.gameControl.trap1CostToFix = Convert.ToInt32(trap1RepairCostText.text);
        GameController.gameControl.trap1AmountFixed = Convert.ToInt32(trap1AmountFixedText.text);
        GameController.gameControl.trap1IsUnlocked = trap1IsUnlockedToggle.isOn;

        #endregion

        //Wave Settings
        GameController.gameControl.initTotalToSpawn = Convert.ToInt32(waveSpawnCountText.text);
        GameController.gameControl.startingWave = Convert.ToInt32(waveStartingWaveText.text);

        //Wall Settings
        GameController.gameControl.wallHealth = Convert.ToInt32(wallHealthText.text);
        GameController.gameControl.wallCostToFix = Convert.ToInt32(wallRepairCostText.text);
        GameController.gameControl.wallAmountFixed = Convert.ToInt32(wallAmountFixedText.text);

        #endregion

        //Save Settings
        GameController.gameControl.Save();
    }

    public void LoadSettings()
    {
        GameController.gameControl.Load();
        LoadGameSettingsUI();
    } 
}
